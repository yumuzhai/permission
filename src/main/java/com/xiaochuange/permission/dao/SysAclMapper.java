package com.xiaochuange.permission.dao;

import com.xiaochuange.permission.beans.PageQuery;
import com.xiaochuange.permission.model.SysAcl;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface SysAclMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(SysAcl record);

    int insertSelective(SysAcl record);

    SysAcl selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(SysAcl record);

    int updateByPrimaryKey(SysAcl record);

    int countByNameAndAclModuleId(@Param("aclModuleId") int aclModuleId,
                                  @Param("name") String name, @Param("id") Integer id);

    // 分页查询，根据权限模块Id查询权限点
    int countByAclModuleId(@Param("aclModuleId") Integer id);

    List<SysAcl> getPageByAclModuleId(@Param("aclModuleId") Integer aclModuleId,
                                      @Param("page") PageQuery pageQuery);

    /**
     * 获取所有的权限点
     * @return
     */
    List<SysAcl> getAll();

    /**
     * 根据权限点id集合查询权限点列表
     * @param aclIdList
     * @return
     */
    List<SysAcl> getByIdList(@Param("aclIdList") List<Integer> aclIdList);
}