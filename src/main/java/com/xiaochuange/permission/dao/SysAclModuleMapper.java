package com.xiaochuange.permission.dao;

import com.xiaochuange.permission.model.SysAclModule;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface SysAclModuleMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(SysAclModule record);

    int insertSelective(SysAclModule record);

    SysAclModule selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(SysAclModule record);

    int updateByPrimaryKey(SysAclModule record);

    int countByNameAndParentId(@Param("parentId") Integer parentId,
                                   @Param("name") String aclModuleName,
                                   @Param("id") Integer id);

    // 根据父级id查询权限模块总数
    int countByParentId(@Param("aclModuleId") Integer id);

    // 根据层级查询
    List<SysAclModule> getChildAclModuleListByLevel(@Param("level") String level);

    // 批量更新权限模块
    void batchUpdateLevel(@Param("sysAclModuleList") List<SysAclModule> aclModuleList);

    // 获取所有权限模块
    List<SysAclModule> getAllAclModule();
}