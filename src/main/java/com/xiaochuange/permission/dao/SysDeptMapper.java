package com.xiaochuange.permission.dao;

import com.xiaochuange.permission.model.SysDept;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface SysDeptMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(SysDept record);

    int insertSelective(SysDept record);

    SysDept selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(SysDept record);

    int updateByPrimaryKey(SysDept record);

    // 查询所有部门列表
    List<SysDept> getAllDept();

    // 根据部门层级查询子部门信息
    List<SysDept> getChildDeptListByLevel(@Param("level") String level);

    // 批量更新level
    Integer batchUpdateLevel(@Param("sysDeptList") List<SysDept> sysDeptList);

    // 根据父id，部门名称，部门id
    Integer countByNameAndParentId(@Param("parentId") Integer parentId,
                                   @Param("name") String name, @Param("id") Integer id);

}