package com.xiaochuange.permission.dao;

import com.xiaochuange.permission.model.SysRole;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface SysRoleMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(SysRole record);

    int insertSelective(SysRole record);

    SysRole selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(SysRole record);

    int updateByPrimaryKey(SysRole record);

    // 根据角色名称查询角色是否存在
    int countByName(@Param("name") String name, @Param("id") Integer id);

    // 查询所有角色列表
    List<SysRole> getAll();
}