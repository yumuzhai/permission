package com.xiaochuange.permission.dao;

import com.xiaochuange.permission.model.SysRoleUser;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface SysRoleUserMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(SysRoleUser record);

    int insertSelective(SysRoleUser record);

    SysRoleUser selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(SysRoleUser record);

    int updateByPrimaryKey(SysRoleUser record);

    /**
     * 根据userId查询角色列表
     * @param userId 用户id
     * @return
     */
    List<Integer> getRoleIdListByUserId(@Param("userId") int userId);
}