package com.xiaochuange.permission.dao;

import com.xiaochuange.permission.beans.PageQuery;
import com.xiaochuange.permission.model.SysUser;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface SysUserMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(SysUser record);

    int insertSelective(SysUser record);

    SysUser selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(SysUser record);

    int updateByPrimaryKey(SysUser record);

    Integer countByEmail(@Param("email") String email, @Param("id") Integer userId);

    Integer countByTelephone(@Param("telephone") String telephone, @Param("id") Integer userId);

    SysUser findByKeyword(@Param("keyword") String keyword);

    // 根据部门id查询部门数量
    Integer countByDeptId(@Param("deptId") int deptId);

    // 根据部门id分页查询用户信息
    List<SysUser> getPageByDeptId(@Param("deptId") int deptId, @Param("page") PageQuery page);

    // 根据用户id集合查询用户列表
    List<SysUser> getByIdList(@Param("idList") List<Integer> idList);

    // 查询所有用户信息
    List<SysUser> getAll();

}