package com.xiaochuange.permission.dto;

import com.xiaochuange.permission.model.SysAcl;
import org.springframework.beans.BeanUtils;

public class AclDto extends SysAcl {

    // 是否要默认选中
    private boolean checked = false;

    // ? 这里有一段说明，后续可以再来看
    // 是否有权限操作
    private boolean hasAcl = false;

    public static AclDto adapter(SysAcl sysAcl) {
        AclDto aclDto = new AclDto();
        BeanUtils.copyProperties(sysAcl, aclDto);
        return aclDto;
    }

    public boolean isChecked() {
        return checked;
    }

    public void setChecked(boolean checked) {
        this.checked = checked;
    }

    public boolean isHasAcl() {
        return hasAcl;
    }

    public void setHasAcl(boolean hasAcl) {
        this.hasAcl = hasAcl;
    }

    @Override
    public String toString() {
        return "AclDto{" +
                "checked=" + checked +
                ", hasAcl=" + hasAcl +
                '}';
    }
}
