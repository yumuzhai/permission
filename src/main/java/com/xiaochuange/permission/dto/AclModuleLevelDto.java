package com.xiaochuange.permission.dto;

import com.google.common.collect.Lists;
import com.xiaochuange.permission.model.SysAclModule;
import org.springframework.beans.BeanUtils;
import java.util.List;

public class AclModuleLevelDto extends SysAclModule {

    private List<AclModuleLevelDto> aclModuleList = Lists.newArrayList();

    // 权限点Dto集合
    private List<AclDto> aclList = Lists.newArrayList();

    public static AclModuleLevelDto adapter(SysAclModule sysAclModule) {
        AclModuleLevelDto aclModuleLevelDto = new AclModuleLevelDto();
        BeanUtils.copyProperties(sysAclModule, aclModuleLevelDto);
        return aclModuleLevelDto;
    }

    public List<AclModuleLevelDto> getAclModuleList() {
        return aclModuleList;
    }

    public void setAclModuleList(List<AclModuleLevelDto> aclModuleList) {
        this.aclModuleList = aclModuleList;
    }

    public List<AclDto> getAclList() {
        return aclList;
    }

    public void setAclList(List<AclDto> aclList) {
        this.aclList = aclList;
    }

    @Override
    public String toString() {
        return "AclModuleLevelDto{" +
                "aclModuleList=" + aclModuleList +
                ", aclList=" + aclList +
                '}';
    }
}
