package com.xiaochuange.permission.model;

import lombok.EqualsAndHashCode;
import java.util.Date;

// 含义是在做equals()和hashCode()时，完全取决于id
//@EqualsAndHashCode(of = {"id"})
public class SysAcl {
    private Integer id;

    private String code;

    private String name;

    private Integer aclModuleId;

    private String url;

    private Integer type;

    private Integer status;

    private Integer seq;

    private String remark;

    private String operator;

    private Date operateTime;

    private String operateIp;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code == null ? null : code.trim();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    public Integer getAclModuleId() {
        return aclModuleId;
    }

    public void setAclModuleId(Integer aclModuleId) {
        this.aclModuleId = aclModuleId;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url == null ? null : url.trim();
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getSeq() {
        return seq;
    }

    public void setSeq(Integer seq) {
        this.seq = seq;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark == null ? null : remark.trim();
    }

    public String getOperator() {
        return operator;
    }

    public void setOperator(String operator) {
        this.operator = operator == null ? null : operator.trim();
    }

    public Date getOperateTime() {
        return operateTime;
    }

    public void setOperateTime(Date operateTime) {
        this.operateTime = operateTime;
    }

    public String getOperateIp() {
        return operateIp;
    }

    public void setOperateIp(String operateIp) {
        this.operateIp = operateIp == null ? null : operateIp.trim();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SysAcl sysAcl = (SysAcl) o;

        if (id != null ? !id.equals(sysAcl.id) : sysAcl.id != null) return false;
        if (code != null ? !code.equals(sysAcl.code) : sysAcl.code != null) return false;
        if (name != null ? !name.equals(sysAcl.name) : sysAcl.name != null) return false;
        if (aclModuleId != null ? !aclModuleId.equals(sysAcl.aclModuleId) : sysAcl.aclModuleId != null) return false;
        if (url != null ? !url.equals(sysAcl.url) : sysAcl.url != null) return false;
        if (type != null ? !type.equals(sysAcl.type) : sysAcl.type != null) return false;
        if (status != null ? !status.equals(sysAcl.status) : sysAcl.status != null) return false;
        if (seq != null ? !seq.equals(sysAcl.seq) : sysAcl.seq != null) return false;
        if (remark != null ? !remark.equals(sysAcl.remark) : sysAcl.remark != null) return false;
        if (operator != null ? !operator.equals(sysAcl.operator) : sysAcl.operator != null) return false;
        if (operateTime != null ? !operateTime.equals(sysAcl.operateTime) : sysAcl.operateTime != null) return false;
        return operateIp != null ? operateIp.equals(sysAcl.operateIp) : sysAcl.operateIp == null;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (code != null ? code.hashCode() : 0);
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (aclModuleId != null ? aclModuleId.hashCode() : 0);
        result = 31 * result + (url != null ? url.hashCode() : 0);
        result = 31 * result + (type != null ? type.hashCode() : 0);
        result = 31 * result + (status != null ? status.hashCode() : 0);
        result = 31 * result + (seq != null ? seq.hashCode() : 0);
        result = 31 * result + (remark != null ? remark.hashCode() : 0);
        result = 31 * result + (operator != null ? operator.hashCode() : 0);
        result = 31 * result + (operateTime != null ? operateTime.hashCode() : 0);
        result = 31 * result + (operateIp != null ? operateIp.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "SysAcl{" +
                "id=" + id +
                ", code='" + code + '\'' +
                ", name='" + name + '\'' +
                ", aclModuleId=" + aclModuleId +
                ", url='" + url + '\'' +
                ", type=" + type +
                ", status=" + status +
                ", seq=" + seq +
                ", remark='" + remark + '\'' +
                ", operator='" + operator + '\'' +
                ", operateTime=" + operateTime +
                ", operateIp='" + operateIp + '\'' +
                '}';
    }
}