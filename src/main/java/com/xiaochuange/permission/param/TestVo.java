package com.xiaochuange.permission.param;

import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.NotEmpty;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.util.List;


public class TestVo {

    @NotNull(message = "id不能为空")
    @Min(value = 0, message = "id至少大于等于0")
    @Max(value = 10, message = "id不能大于10")
    private Integer id;

    @NotBlank
    private String msg;

    /*@NotEmpty
    private List<String> strList;*/

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}
