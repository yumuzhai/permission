package com.xiaochuange.permission.service;

import com.xiaochuange.permission.param.AclModuleParam;

public interface SysAclModuleService {

    void saveAclModule(AclModuleParam param);

    void updateAclModule(AclModuleParam param);

    void delete(int aclModuleId);
}
