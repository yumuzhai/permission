package com.xiaochuange.permission.service;

import com.xiaochuange.permission.beans.PageQuery;
import com.xiaochuange.permission.beans.PageResult;
import com.xiaochuange.permission.model.SysAcl;
import com.xiaochuange.permission.param.AclParam;

public interface SysAclService {

    void saveAcl(AclParam param);

    void updateAcl(AclParam param);

    // 分页查询
    PageResult<SysAcl> getPageByAclModuleId(Integer aclModuleId, PageQuery pageQuery);
}
