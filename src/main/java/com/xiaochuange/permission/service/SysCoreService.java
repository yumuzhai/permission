package com.xiaochuange.permission.service;

import com.xiaochuange.permission.model.SysAcl;
import java.util.List;

public interface SysCoreService {

    // 当前用户已分配的权限点
    List<SysAcl> getCurrentUserAclList();

    // 当前角色分配的权限点
    List<SysAcl> getRoleAclList(int roleId);

}
