package com.xiaochuange.permission.service;

import com.xiaochuange.permission.param.DeptParam;

public interface SysDeptService {
    Integer saveDept(DeptParam param);

    void updateDept(DeptParam param);
}
