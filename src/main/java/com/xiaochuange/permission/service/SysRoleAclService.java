package com.xiaochuange.permission.service;

import java.util.List;

public interface SysRoleAclService {

    /**
     * 根据角色id和权限id集合 修改 角色权限信息
     * @param roleId
     * @param aclIdList
     */
    void changeRoleAcls(int roleId, List<Integer> aclIdList);

}
