package com.xiaochuange.permission.service;

import com.xiaochuange.permission.model.SysRole;
import com.xiaochuange.permission.param.RoleParam;

import java.util.List;

public interface SysRoleService {

    void saveRole(RoleParam param);

    void updateRole(RoleParam param);

    // 查询所有角色列表
    List<SysRole> getAll();
}
