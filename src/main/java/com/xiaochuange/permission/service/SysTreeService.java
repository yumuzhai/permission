package com.xiaochuange.permission.service;

import com.xiaochuange.permission.dto.AclModuleLevelDto;
import com.xiaochuange.permission.dto.DeptLevelDto;

import java.util.List;

public interface SysTreeService {

    /**
     * 生成部门树
     * @return
     */
    List<DeptLevelDto> deptTree();

    /**
     * 生成权限模块树
     * @return
     */
    List<AclModuleLevelDto> aclModuleTree();

    /**
     * 生成角色权限树
     * @param roleId
     * @return
     */
    List<AclModuleLevelDto> roleTree(int roleId);
}
