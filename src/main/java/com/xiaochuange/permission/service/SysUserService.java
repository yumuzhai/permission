package com.xiaochuange.permission.service;

import com.xiaochuange.permission.beans.PageQuery;
import com.xiaochuange.permission.beans.PageResult;
import com.xiaochuange.permission.model.SysUser;
import com.xiaochuange.permission.param.UserParam;

public interface SysUserService {

    void saveUser(UserParam param);

    // 后台管理系统使用修改
    void updateUser(UserParam param);

    SysUser findByKeyword(String keyword);

    // 根据部门id分页查询用户列表
    PageResult<SysUser> getPageByDeptId(int deptId, PageQuery page);
}
