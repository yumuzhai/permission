package com.xiaochuange.permission.service.impl;

import com.google.common.base.Preconditions;
import com.xiaochuange.permission.common.RequestHolder;
import com.xiaochuange.permission.dao.SysAclMapper;
import com.xiaochuange.permission.dao.SysAclModuleMapper;
import com.xiaochuange.permission.exception.ParamException;
import com.xiaochuange.permission.model.SysAclModule;
import com.xiaochuange.permission.model.SysDept;
import com.xiaochuange.permission.param.AclModuleParam;
import com.xiaochuange.permission.service.SysAclModuleService;
import com.xiaochuange.permission.util.BeanValidator;
import com.xiaochuange.permission.util.IpUtil;
import com.xiaochuange.permission.util.LevelUtil;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

@Service
public class SysAclModuleServiceImpl implements SysAclModuleService {

    @Autowired
    private SysAclModuleMapper sysAclModuleMapper;
    @Autowired
    private SysAclMapper sysAclMapper;

    @Override
    public void saveAclModule(AclModuleParam param) {
        BeanValidator.check(param);
        if (checkExist(param.getParentId(), param.getName(), param.getId())) {
            throw new ParamException("同一层级下存在相同名称的权限模块");
        }
        SysAclModule aclModule = new SysAclModule();
        aclModule.setName(param.getName());
        aclModule.setParentId(param.getParentId());
        aclModule.setSeq(param.getSeq());
        aclModule.setStatus(param.getStatus());
        aclModule.setRemark(param.getRemark());

        aclModule.setLevel(LevelUtil.calculateLevel(getLevel(param.getParentId()), param.getParentId()));
        aclModule.setOperator(RequestHolder.getCurrentUser().getUsername());
        aclModule.setOperateIp(IpUtil.getRemoteIp(RequestHolder.getCurrentRequest()));
        aclModule.setOperateTime(new Date());
        sysAclModuleMapper.insertSelective(aclModule);
        //sysLogService.saveAclModuleLog(null, aclModule);
    }

    @Override
    public void updateAclModule(AclModuleParam param) {
        BeanValidator.check(param);
        if (checkExist(param.getParentId(), param.getName(), param.getId())) {
            throw new ParamException("同一层级下存在相同名称的权限模块");
        }
        SysAclModule before = sysAclModuleMapper.selectByPrimaryKey(param.getId());
        Preconditions.checkNotNull(before, "待更新的权限模块不存在");

        SysAclModule after = new SysAclModule();
        after.setId(param.getId());
        after.setName(param.getName());
        after.setParentId(param.getParentId());
        after.setSeq(param.getSeq());
        after.setStatus(param.getStatus());
        after.setRemark(param.getRemark());

        after.setLevel(LevelUtil.calculateLevel(getLevel(param.getParentId()), param.getParentId()));
        after.setOperator(RequestHolder.getCurrentUser().getUsername());
        after.setOperateIp(IpUtil.getRemoteIp(RequestHolder.getCurrentRequest()));
        after.setOperateTime(new Date());
        updateWithChild(before, after);
        //sysLogService.saveAclModuleLog(before, after);
    }

    @Transactional
    public void updateWithChild(SysAclModule before, SysAclModule after){
        String newLevelPrefix = after.getLevel();
        String oldLevelPrefix = before.getLevel();
        if (!after.getLevel().equals(before.getLevel())) {
            List<SysAclModule> sysAclModuleList = sysAclModuleMapper.getChildAclModuleListByLevel(before.getLevel());
            if (CollectionUtils.isNotEmpty(sysAclModuleList)) {
                for (SysAclModule sysAclModule : sysAclModuleList) {
                    String level = sysAclModule.getLevel();
                    // indexOf(str);返回指定子字符串在此字符串中第一次出现处的索引
                    if (level.indexOf(oldLevelPrefix) == 0) {
                        level = newLevelPrefix + level.substring(oldLevelPrefix.length());
                        sysAclModule.setLevel(level);
                    }
                }
                sysAclModuleMapper.batchUpdateLevel(sysAclModuleList);
            }
        }
        sysAclModuleMapper.updateByPrimaryKey(after);
    }

//    // 如果要保证事务生效，需要调整这个方法，一个可行的方法是重新创建一个service类，然后把这个方法转移过去
//    @Transactional
//    public void updateWithChild(SysAclModule before, SysAclModule after) {
//        String newLevelPrefix = after.getLevel();
//        String oldLevelPrefix = before.getLevel();
//        if (!after.getLevel().equals(before.getLevel())) {
//            // 当前的层级
//            String curLevel = before.getLevel() + "." + before.getId();
//            // List<SysAclModule> aclModuleList = sysAclModuleMapper.getChildAclModuleListByLevel(curLevel + "%");
//            // 根据当前层级查询权限模块列表
//            List<SysAclModule> aclModuleList = sysAclModuleMapper.getChildAclModuleListByLevel(curLevel);
//            if (CollectionUtils.isNotEmpty(aclModuleList)) {
//                for (SysAclModule aclModule : aclModuleList) {
//                    String level = aclModule.getLevel();
//                    // indexOf(str);返回指定子字符串在此字符串中第一次出现处的索引。
//                    if (level.equals(curLevel) || level.indexOf(curLevel + ".") == 0) {
//                        // getChildAclModuleListByLevel可能会取出多余的内容，因此需要加个判断
//                        // 比如0.1*可能取出0.1、0.1.3、0.11、0.11.3，而期望取出0.1、0.1.3，因此呢需要判断等于0.1或者以0.1.为前缀才满足条件
//                        level = newLevelPrefix + level.substring(oldLevelPrefix.length());
//                        aclModule.setLevel(level);
//                    }
//                }
//                sysAclModuleMapper.batchUpdateLevel(aclModuleList);
//            }
//        }
//        sysAclModuleMapper.updateByPrimaryKeySelective(after);
//    }

    /**
     * 校验权限模块名称是否存在
     *
     * @param parentId
     * @param aclModuleName
     * @param deptId
     * @return
     */
    private boolean checkExist(Integer parentId, String aclModuleName, Integer deptId) {
        return sysAclModuleMapper.countByNameAndParentId(parentId, aclModuleName, deptId) > 0;
    }

    /**
     * 获取层级
     *
     * @param aclModuleId
     * @return
     */
    private String getLevel(Integer aclModuleId) {
        SysAclModule aclModule = sysAclModuleMapper.selectByPrimaryKey(aclModuleId);
        if (aclModule == null) {
            return null;
        }
        return aclModule.getLevel();
    }

    @Override
    public void delete(int aclModuleId) {
        SysAclModule aclModule = sysAclModuleMapper.selectByPrimaryKey(aclModuleId);
        Preconditions.checkNotNull(aclModule, "待删除的权限模块不存在，无法删除");
        if(sysAclModuleMapper.countByParentId(aclModule.getId()) > 0) {
            throw new ParamException("当前模块下面有子模块，无法删除");
        }
        if (sysAclMapper.countByAclModuleId(aclModule.getId()) > 0) {
            throw new ParamException("当前模块下面有用户，无法删除");
        }
        sysAclModuleMapper.deleteByPrimaryKey(aclModuleId);
    }
}
