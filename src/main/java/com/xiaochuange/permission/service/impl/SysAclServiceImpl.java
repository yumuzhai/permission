package com.xiaochuange.permission.service.impl;

import com.google.common.base.Preconditions;
import com.xiaochuange.permission.beans.PageQuery;
import com.xiaochuange.permission.beans.PageResult;
import com.xiaochuange.permission.common.RequestHolder;
import com.xiaochuange.permission.dao.SysAclMapper;
import com.xiaochuange.permission.exception.ParamException;
import com.xiaochuange.permission.model.SysAcl;
import com.xiaochuange.permission.param.AclParam;
import com.xiaochuange.permission.service.SysAclService;
import com.xiaochuange.permission.util.BeanValidator;
import com.xiaochuange.permission.util.IpUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@Service
public class SysAclServiceImpl implements SysAclService {

    @Autowired
    private SysAclMapper sysAclMapper;

    @Override
    public void saveAcl(AclParam param) {
        BeanValidator.check(param);
        if (checkExist(param.getAclModuleId(), param.getName(), param.getId())) {
            throw new ParamException("当前权限模块下面存在相同名称的权限点");
        }
        SysAcl acl = new SysAcl();
        acl.setName(param.getName());
        acl.setAclModuleId(param.getAclModuleId());
        acl.setUrl(param.getUrl());
        acl.setType(param.getType());
        acl.setStatus(param.getStatus());
        acl.setSeq(param.getSeq());
        acl.setRemark(param.getRemark());

        acl.setCode(generateCode());
        acl.setOperator(RequestHolder.getCurrentUser().getUsername());
        acl.setOperateIp(IpUtil.getRemoteIp(RequestHolder.getCurrentRequest()));
        acl.setOperateTime(new Date());
        sysAclMapper.insertSelective(acl);
        //sysLogService.saveAclLog(null, acl);
    }

    @Override
    public void updateAcl(AclParam param) {
        BeanValidator.check(param);
        if (checkExist(param.getAclModuleId(), param.getName(), param.getId())) {
            throw new ParamException("当前权限模块下面存在相同名称的权限点");
        }
        SysAcl before = sysAclMapper.selectByPrimaryKey(param.getId());
        Preconditions.checkNotNull(before, "待更新的权限点不存在");

        SysAcl after = new SysAcl();
        after.setId(param.getId());
        after.setName(param.getName());
        after.setAclModuleId(param.getAclModuleId());
        after.setUrl(param.getUrl());
        after.setType(param.getType());
        after.setStatus(param.getStatus());
        after.setSeq(param.getSeq());
        after.setRemark(param.getRemark());
        //after.setCode(generateCode());

        after.setOperator(RequestHolder.getCurrentUser().getUsername());
        after.setOperateIp(IpUtil.getRemoteIp(RequestHolder.getCurrentRequest()));
        after.setOperateTime(new Date());
        sysAclMapper.updateByPrimaryKeySelective(after);
        //sysLogService.saveAclLog(before, after);
    }

    /**
     * 当前权限模块下面存在相同名称的权限点
     * @param aclModuleId
     * @param name
     * @param id
     * @return
     */
    public boolean checkExist(int aclModuleId, String name, Integer id) {
        return sysAclMapper.countByNameAndAclModuleId(aclModuleId, name, id) > 0;
    }

    /**
     * 生成权限点唯一Code值
     * @return
     */
    public String generateCode() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMddHHmmss");
        return dateFormat.format(new Date()) + "_" + (int) (Math.random() * 100);
    }

    @Override
    public PageResult<SysAcl> getPageByAclModuleId(Integer aclModuleId, PageQuery pageQuery) {
        BeanValidator.check(pageQuery);
        int count = sysAclMapper.countByAclModuleId(aclModuleId);
        PageResult<SysAcl> pageResult = new PageResult<>();
        if (count > 0) {
            List<SysAcl> aclList = sysAclMapper.getPageByAclModuleId(aclModuleId, pageQuery);
            pageResult.setData(aclList);
            pageResult.setTotal(count);
            return pageResult;
        }
        return pageResult;
    }
}
