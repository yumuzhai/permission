package com.xiaochuange.permission.service.impl;

import com.google.common.base.Preconditions;
import com.xiaochuange.permission.common.RequestHolder;
import com.xiaochuange.permission.dao.SysDeptMapper;
import com.xiaochuange.permission.exception.ParamException;
import com.xiaochuange.permission.model.SysDept;
import com.xiaochuange.permission.param.DeptParam;
import com.xiaochuange.permission.service.SysDeptService;
import com.xiaochuange.permission.util.BeanValidator;
import com.xiaochuange.permission.util.IpUtil;
import com.xiaochuange.permission.util.LevelUtil;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import sun.misc.Request;

import java.util.Date;
import java.util.List;

@Service
public class SysDeptServiceImpl implements SysDeptService {

    @Autowired
    private SysDeptMapper sysDeptMapper;

    @Override
    public Integer saveDept(DeptParam param) {
        BeanValidator.check(param);
        if (checkExist(param.getParentId(), param.getName(), param.getId())) {
            throw new ParamException("同一层级下存在相同名称的部门");
        }
        SysDept sysDept = new SysDept();
        sysDept.setName(param.getName());
        sysDept.setParentId(param.getParentId());
        sysDept.setSeq(param.getSeq());
        sysDept.setRemark(param.getRemark());
        sysDept.setLevel(LevelUtil.calculateLevel(getLevel(param.getParentId()), param.getParentId()));
        //sysDept.setOperator("system");
        //sysDept.setOperateIp("127.0.0.1");
        sysDept.setOperator(RequestHolder.getCurrentUser().getUsername());
        sysDept.setOperateIp(IpUtil.getRemoteIp(RequestHolder.getCurrentRequest()));
        sysDept.setOperateTime(new Date());
        return sysDeptMapper.insertSelective(sysDept);
    }

    // 检查部门是否存在
    private boolean checkExist(Integer parentId, String deptName, Integer deptId) {
        return sysDeptMapper.countByNameAndParentId(parentId, deptName, deptId) > 0;
    }

    private String getLevel(Integer deptId) {
        SysDept sysDept = sysDeptMapper.selectByPrimaryKey(deptId);
        if (sysDept == null) {
            return null;
        }
        return sysDept.getLevel();
    }

    @Override
    public void updateDept(DeptParam param) {
        BeanValidator.check(param);
        if (checkExist(param.getParentId(), param.getName(), param.getId())) {
            throw new ParamException("同一层级下存在相同名称的部门");
        }
        // 这里为什么要查出来呢？方便做日志管理
        SysDept before = sysDeptMapper.selectByPrimaryKey(param.getId());
        Preconditions.checkNotNull(before, "待更新的部门不存在");
        /*if (checkExist(param.getParentId(), param.getName(), param.getId())) {
            throw new ParamException("同一层级下存在相同名称的部门");
        }*/
        SysDept after = new SysDept();
        after.setId(param.getId());
        after.setName(param.getName());
        after.setParentId(param.getParentId());
        after.setSeq(param.getSeq());
        after.setRemark(param.getRemark());
        after.setLevel(LevelUtil.calculateLevel(getLevel(param.getParentId()), param.getParentId()));
        //after.setOperator("system-update");
        //after.setOperateIp("127.0.0.1");
        after.setOperator(RequestHolder.getCurrentUser().getUsername());
        after.setOperateIp(IpUtil.getRemoteIp(RequestHolder.getCurrentRequest()));
        after.setOperateTime(new Date());
        // 更新部门信息，同时更新子部门的层级
        updateWithChild(before, after);
    }

    // 事务，要么都成功，要么都失败
    @Transactional
    public void updateWithChild(SysDept before, SysDept after) {
        String newLevelPrefix = after.getLevel();
        String oldLevelPrefix = before.getLevel();
        if (!after.getLevel().equals(before.getLevel())) {
            List<SysDept> sysDeptList = sysDeptMapper.getChildDeptListByLevel(before.getLevel());
            if (CollectionUtils.isNotEmpty(sysDeptList)) {
                for (SysDept sysDept : sysDeptList) {
                    String level = sysDept.getLevel();
                    //
                    if (level.indexOf(oldLevelPrefix) == 0) {
                        level = newLevelPrefix + level.substring(oldLevelPrefix.length());
                        sysDept.setLevel(level);
                    }
                }
                sysDeptMapper.batchUpdateLevel(sysDeptList);
            }
        }
        sysDeptMapper.updateByPrimaryKey(after);
    }


}
