package com.xiaochuange.permission.service.impl;

import com.google.common.base.Preconditions;
import com.xiaochuange.permission.common.RequestHolder;
import com.xiaochuange.permission.dao.SysRoleMapper;
import com.xiaochuange.permission.exception.ParamException;
import com.xiaochuange.permission.model.SysRole;
import com.xiaochuange.permission.param.RoleParam;
import com.xiaochuange.permission.service.SysRoleService;
import com.xiaochuange.permission.util.BeanValidator;
import com.xiaochuange.permission.util.IpUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.Date;
import java.util.List;


@Service
public class SysRoleServiceImpl implements SysRoleService {

    @Autowired
    private SysRoleMapper sysRoleMapper;

    @Override
    public void saveRole(RoleParam param) {
        BeanValidator.check(param);
        if (checkExist(param.getName(), param.getId())) {
            throw new ParamException("角色名称已经存在");
        }
        SysRole role = new SysRole();
        role.setName(param.getName());
        role.setStatus(param.getStatus());
        role.setType(param.getType());
        role.setRemark(param.getRemark());

        role.setOperator(RequestHolder.getCurrentUser().getUsername());
        role.setOperateIp(IpUtil.getRemoteIp(RequestHolder.getCurrentRequest()));
        role.setOperateTime(new Date());
        sysRoleMapper.insertSelective(role);
        //sysLogService.saveRoleLog(null, role);
    }

    @Override
    public void updateRole(RoleParam param) {
        BeanValidator.check(param);
        if (checkExist(param.getName(), param.getId())) {
            throw new ParamException("角色名称已经存在");
        }
        SysRole before = sysRoleMapper.selectByPrimaryKey(param.getId());
        Preconditions.checkNotNull(before, "待更新的角色不存在");

        SysRole after = new SysRole();
        after.setId(param.getId());
        after.setName(param.getName());
        after.setStatus(param.getStatus());
        after.setType(param.getType());
        after.setRemark(param.getRemark());

        after.setOperator(RequestHolder.getCurrentUser().getUsername());
        after.setOperateIp(IpUtil.getRemoteIp(RequestHolder.getCurrentRequest()));
        after.setOperateTime(new Date());
        sysRoleMapper.updateByPrimaryKeySelective(after);
        //sysLogService.saveRoleLog(before, after);
    }

    /**
     * 校验角色名称是否存在
     * @param name
     * @param id
     * @return
     */
    private boolean checkExist(String name, Integer id) {
        return sysRoleMapper.countByName(name, id) > 0;
    }

    @Override
    public List<SysRole> getAll() {
        return sysRoleMapper.getAll();
    }

}
