package com.xiaochuange.permission.service.impl;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Lists;
import com.google.common.collect.Multimap;
import com.xiaochuange.permission.dao.SysAclMapper;
import com.xiaochuange.permission.dao.SysAclModuleMapper;
import com.xiaochuange.permission.dao.SysDeptMapper;
import com.xiaochuange.permission.dto.AclDto;
import com.xiaochuange.permission.dto.AclModuleLevelDto;
import com.xiaochuange.permission.dto.DeptLevelDto;
import com.xiaochuange.permission.model.SysAcl;
import com.xiaochuange.permission.model.SysAclModule;
import com.xiaochuange.permission.model.SysDept;
import com.xiaochuange.permission.service.SysCoreService;
import com.xiaochuange.permission.service.SysTreeService;
import com.xiaochuange.permission.util.LevelUtil;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class SysTreeServiceImpl implements SysTreeService {

    @Autowired
    private SysDeptMapper sysDeptMapper;
    @Autowired
    private SysAclModuleMapper sysAclModuleMapper;
    @Autowired
    private SysAclMapper sysAclMapper;
    @Autowired
    private SysCoreService sysCoreService;

    /**
     * 整体思路分析：
     * 1）查询出所有的部门信息；
     * 2）将部门集合转换为部门Dto集合；
     * 3）循环遍历部门Dto集合，得到一级部门集合 and Multimap
     * 以level为key，
     * Map<String, List<DeptLevelDto>> -> Multimap<String, DeptLevelDto>为value
     * 4）对一级部门集合排序；
     * 5）调用transformDeptTree()方法递归处理数据；
     *
     * @return
     */
    @Override
    public List<DeptLevelDto> deptTree() {
        // 查询到所有的部门信息
        List<SysDept> sysDeptList = sysDeptMapper.getAllDept();
        // 将部门集合转换为部门Dto集合
        List<DeptLevelDto> deptLevelDtoList = Lists.newArrayList();
        for (SysDept sysDept : sysDeptList) {
            DeptLevelDto deptLevelDto = DeptLevelDto.adapter(sysDept);
            deptLevelDtoList.add(deptLevelDto);
        }
        return deptDtoListToTree(deptLevelDtoList);
    }

    public List<DeptLevelDto> deptDtoListToTree(List<DeptLevelDto> deptLevelDtoList) {
        if (CollectionUtils.isEmpty(deptLevelDtoList)) {
            return Lists.newArrayList();
        }
        // level -> [dept1, dept2,...] ==> Map<String, List<DeptLevelDto>>
        // 0 -> 技术部，产品部，客服部 0.1 -> 后端开发，前端开发，UI设计
        Multimap<String, DeptLevelDto> deptLevelDtoMultimap = ArrayListMultimap.create();
        // 获取一级部门
        List<DeptLevelDto> rootList = Lists.newArrayList();
        for (DeptLevelDto deptLevelDto : deptLevelDtoList) {
            //
            deptLevelDtoMultimap.put(deptLevelDto.getLevel(), deptLevelDto);
            // 组装一级部门集合
            if (LevelUtil.ROOT.equals(deptLevelDto.getLevel())) {
                rootList.add(deptLevelDto);
            }
        }

        // 按照seq从小到大排序
        Collections.sort(rootList, new Comparator<DeptLevelDto>() {
            @Override
            public int compare(DeptLevelDto o1, DeptLevelDto o2) {
                return o1.getSeq() - o2.getSeq();
            }
        });

        transformDeptTree(rootList, LevelUtil.ROOT, deptLevelDtoMultimap);
        return rootList;
    }

    // 递归生成树
    // level:0,0, all 0->0.1,0.2
    // level:0.1
    // level:0.2
    public void transformDeptTree(List<DeptLevelDto> deptLevelDtoList,
                                  String level, Multimap<String, DeptLevelDto> deptLevelDtoMultimap) {
        for (int i = 0; i < deptLevelDtoList.size(); i++) {
            // 遍历该层的每个元素
            // 技术部
            DeptLevelDto deptLevelDto = deptLevelDtoList.get(i);
            // 处理当前层级的数据
            // 0 -> 0.1, 0.1 -> 0.1.2
            String nextLevel = LevelUtil.calculateLevel(level, deptLevelDto.getId());
            // 处理下一层
            // 0.1 -> 后端开发，前端开发，UI设计
            List<DeptLevelDto> tempDeptLevelDtoList = (List<DeptLevelDto>) deptLevelDtoMultimap.get(nextLevel);
            if (CollectionUtils.isNotEmpty(tempDeptLevelDtoList)) {
                // 排序
                Collections.sort(tempDeptLevelDtoList, deptSeqComparator);
                // 设置下一层部门
                deptLevelDto.setDeptList(tempDeptLevelDtoList);
                // 进入到下一层处理
                transformDeptTree(tempDeptLevelDtoList, nextLevel, deptLevelDtoMultimap);
            }
        }
    }

    // 排序
    public Comparator<DeptLevelDto> deptSeqComparator = new Comparator<DeptLevelDto>() {
        @Override
        public int compare(DeptLevelDto o1, DeptLevelDto o2) {
            return o1.getSeq() - o2.getSeq();
        }
    };

    // ============================ 权限模块树 ============================
    @Override
    public List<AclModuleLevelDto> aclModuleTree() {
        // 获取所有权限模块
        List<SysAclModule> aclModuleList = sysAclModuleMapper.getAllAclModule();

        // 把权限模块列表适配为一个权限Dto列表
        List<AclModuleLevelDto> dtoList = Lists.newArrayList();
        for (SysAclModule aclModule : aclModuleList) {
            dtoList.add(AclModuleLevelDto.adapter(aclModule));
        }
        return aclModuleListToTree(dtoList);
    }

    public List<AclModuleLevelDto> aclModuleListToTree(List<AclModuleLevelDto> dtoList) {
        if (CollectionUtils.isEmpty(dtoList)) {
            return Lists.newArrayList();
        }
        // level -> [aclModule1, aclModule2, ...] ==> Map<String, List<Object>>
        Multimap<String, AclModuleLevelDto> levelAclModuleMap = ArrayListMultimap.create();
        List<AclModuleLevelDto> rootList = Lists.newArrayList();
        // 遍历集合，组装首层列表 and 按照level组装的Map
        for (AclModuleLevelDto dto : dtoList) {
            levelAclModuleMap.put(dto.getLevel(), dto);
            if (LevelUtil.ROOT.equals(dto.getLevel())) {
                rootList.add(dto);
            }
        }
        // 首层排序
        Collections.sort(rootList, aclModuleSeqComparator);
        transformAclModuleTree(rootList, LevelUtil.ROOT, levelAclModuleMap);
        return rootList;
    }

    /**
     * 递归操作，按照层级层层遍历
     *
     * @param dtoList           List<AclModuleLevelDto>
     * @param level             层级
     * @param levelAclModuleMap Multimap
     */
    public void transformAclModuleTree(List<AclModuleLevelDto> dtoList, String level,
                                       Multimap<String, AclModuleLevelDto> levelAclModuleMap) {
        for (int i = 0; i < dtoList.size(); i++) {
            // 拿到当前的AclModuleLevelDto
            AclModuleLevelDto dto = dtoList.get(i);
            // 获取下一个层级
            String nextLevel = LevelUtil.calculateLevel(level, dto.getId());
            // 从Multimap中获取下一层级的权限模块列表
            List<AclModuleLevelDto> tempList = (List<AclModuleLevelDto>) levelAclModuleMap.get(nextLevel);
            // 如果存在才处理，不存在进入下一次循环
            if (CollectionUtils.isNotEmpty(tempList)) {
                // 下一层排序
                Collections.sort(tempList, aclModuleSeqComparator);
                // 设置到当前层的下一层列表
                dto.setAclModuleList(tempList);
                // 递归调用
                transformAclModuleTree(tempList, nextLevel, levelAclModuleMap);
            }
        }
    }

    public Comparator<AclModuleLevelDto> aclModuleSeqComparator = new Comparator<AclModuleLevelDto>() {
        public int compare(AclModuleLevelDto o1, AclModuleLevelDto o2) {
            return o1.getSeq() - o2.getSeq();
        }
    };


    // ============================ 角色权限树 ============================

    @Override
    public List<AclModuleLevelDto> roleTree(int roleId) {
//        // 1、当前用户已分配的权限点
//        List<SysAcl> userAclList = sysCoreService.getCurrentUserAclList();
//        // 2、当前角色分配的权限点
//        List<SysAcl> roleAclList = sysCoreService.getRoleAclList(roleId);
//        // JDK 1.8 Stream流操作
//        Set<Integer> userAclIdSet = userAclList.stream().map(sysAcl -> sysAcl.getId()).collect(Collectors.toSet());
//        Set<Integer> roleAclIdSet = roleAclList.stream().map(sysAcl -> sysAcl.getId()).collect(Collectors.toSet());
//
//        Set<SysAcl> aclSet = new HashSet<>(roleAclList);
//        aclSet.addAll(userAclList);
//        List<AclDto> aclDtoList = Lists.newArrayList();
//        for (SysAcl acl : aclSet) {
//            AclDto aclDto = AclDto.adapter(acl);
//            if (userAclIdSet.contains(acl.getId())) {
//                aclDto.setHasAcl(true);
//            }
//            if (roleAclIdSet.contains(acl.getId())) {
//                aclDto.setChecked(true);
//            }
//            aclDtoList.add(aclDto);
//        }

        // 1、当前用户已分配的权限点
        List<SysAcl> userAclList = sysCoreService.getCurrentUserAclList();
        // 2、当前角色分配的权限点
        List<SysAcl> roleAclList = sysCoreService.getRoleAclList(roleId);
        // 3、当前系统所有权限点
        List<AclDto> aclDtoList = Lists.newArrayList();

        // JDK 1.8 Stream流操作
        Set<Integer> userAclIdSet = userAclList.stream().map(sysAcl -> sysAcl.getId()).collect(Collectors.toSet());
        Set<Integer> roleAclIdSet = roleAclList.stream().map(sysAcl -> sysAcl.getId()).collect(Collectors.toSet());
        // 获取所有权限列表
        List<SysAcl> allAclList = sysAclMapper.getAll();
        for (SysAcl sysAcl : allAclList) {
            AclDto aclDto = AclDto.adapter(sysAcl);
            if (userAclIdSet.contains(sysAcl.getId())) {
                aclDto.setHasAcl(true);
            }
            if (roleAclIdSet.contains(sysAcl.getId())) {
                aclDto.setChecked(true);
            }
            aclDtoList.add(aclDto);
        }
        return aclListToTree(aclDtoList);
    }

    /**
     * @param aclDtoList
     * @return
     */
    public List<AclModuleLevelDto> aclListToTree(List<AclDto> aclDtoList) {
        if (CollectionUtils.isEmpty(aclDtoList)) {
            return Lists.newArrayList();
        }
        // 权限模块树
        List<AclModuleLevelDto> aclModuleLevelList = aclModuleTree();
        Multimap<Integer, AclDto> moduleIdAclMap = ArrayListMultimap.create();
        for (AclDto aclDto : aclDtoList) {
            if (aclDto.getStatus() == 1) {
                moduleIdAclMap.put(aclDto.getAclModuleId(), aclDto);
            }
        }
        //
        bindAclsWithOrder(aclModuleLevelList, moduleIdAclMap);
        return aclModuleLevelList;
    }

    /**
     * 递归绑定
     *
     * @param aclModuleLevelList
     * @param moduleIdAclMap
     */
    public void bindAclsWithOrder(List<AclModuleLevelDto> aclModuleLevelList, Multimap<Integer, AclDto> moduleIdAclMap) {
        if (CollectionUtils.isEmpty(aclModuleLevelList)) {
            return;
        }
        for (AclModuleLevelDto aclModuleLevelDto : aclModuleLevelList) {
            List<AclDto> aclDtoList = (List<AclDto>) moduleIdAclMap.get(aclModuleLevelDto.getId());
            if (CollectionUtils.isNotEmpty(aclDtoList)) {
                Collections.sort(aclDtoList, aclSeqComparator);
                aclModuleLevelDto.setAclList(aclDtoList);
            }
            bindAclsWithOrder(aclModuleLevelDto.getAclModuleList(), moduleIdAclMap);
        }
    }

    // 排序
    public Comparator<AclDto> aclSeqComparator = new Comparator<AclDto>() {
        public int compare(AclDto o1, AclDto o2) {
            return o1.getSeq() - o2.getSeq();
        }
    };

}
