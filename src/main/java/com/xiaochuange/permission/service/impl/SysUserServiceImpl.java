package com.xiaochuange.permission.service.impl;

import com.google.common.base.Preconditions;
import com.xiaochuange.permission.beans.PageQuery;
import com.xiaochuange.permission.beans.PageResult;
import com.xiaochuange.permission.common.RequestHolder;
import com.xiaochuange.permission.dao.SysUserMapper;
import com.xiaochuange.permission.exception.ParamException;
import com.xiaochuange.permission.model.SysUser;
import com.xiaochuange.permission.param.UserParam;
import com.xiaochuange.permission.service.SysUserService;
import com.xiaochuange.permission.util.BeanValidator;
import com.xiaochuange.permission.util.IpUtil;
import com.xiaochuange.permission.util.MD5Util;
import com.xiaochuange.permission.util.PasswordUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
public class SysUserServiceImpl implements SysUserService {

    @Autowired
    private SysUserMapper sysUserMapper;

    @Override
    public void saveUser(UserParam param) {
        BeanValidator.check(param);
        if (checkTelephoneExist(param.getTelephone(), param.getId())) {
            throw new ParamException("电话已被占用");
        }
        if (checkEmailExist(param.getMail(), param.getId())) {
            throw new ParamException("邮箱已被占用");
        }
        // 1、通过工具类生成password，通过邮箱告诉用户；2、用户自己输入密码；
        //String password = PasswordUtil.randomPassword();
        String password = "1234";  // 这里直接写死
        String encryptedPassword = MD5Util.encrypt(password);
        SysUser sysUser = new SysUser();
        sysUser.setUsername(param.getUsername());
        sysUser.setTelephone(param.getTelephone());
        sysUser.setMail(param.getMail());
        sysUser.setPassword(encryptedPassword);
        sysUser.setDeptId(param.getDeptId());
        sysUser.setStatus(param.getStatus());
        sysUser.setRemark(param.getRemark());
        //sysUser.setOperator("system");
        //sysUser.setOperateIp("127.0.0.1");
        sysUser.setOperator(RequestHolder.getCurrentUser().getUsername());
        sysUser.setOperateIp(IpUtil.getRemoteIp(RequestHolder.getCurrentRequest()));
        sysUser.setOperateTime(new Date());
        // sendEmail，自己完善
        sysUserMapper.insertSelective(sysUser);
    }

    // 校验邮箱是否存在
    public boolean checkEmailExist(String mail, Integer userId) {
        return sysUserMapper.countByEmail(mail, userId) > 0;
    }

    // 校验电话号码是否存在
    public boolean checkTelephoneExist(String telephone, Integer userId) {
        return sysUserMapper.countByTelephone(telephone, userId) > 0;
    }

    @Override
    public void updateUser(UserParam param) {
        BeanValidator.check(param);
        if (checkTelephoneExist(param.getTelephone(), param.getId())) {
            throw new ParamException("电话已被占用");
        }
        if (checkEmailExist(param.getMail(), param.getId())) {
            throw new ParamException("邮箱已被占用");
        }
        SysUser before = sysUserMapper.selectByPrimaryKey(param.getId());
        Preconditions.checkNotNull(before, "待更新的用户不存在");

        SysUser after = new SysUser();
        after.setId(param.getId());
        after.setUsername(param.getUsername());
        after.setTelephone(param.getTelephone());
        after.setMail(param.getMail());
        after.setDeptId(param.getDeptId());
        after.setStatus(param.getStatus());
        after.setRemark(param.getRemark());
        //after.setOperator("system-update");
        //after.setOperateIp("127.0.0.1");
        after.setOperator(RequestHolder.getCurrentUser().getUsername());
        after.setOperateIp(IpUtil.getRemoteIp(RequestHolder.getCurrentRequest()));
        after.setOperateTime(new Date());
        sysUserMapper.updateByPrimaryKeySelective(after);
    }

    @Override
    public SysUser findByKeyword(String keyword) {
        return sysUserMapper.findByKeyword(keyword);
    }

    public PageResult<SysUser> getPageByDeptId(int deptId, PageQuery page) {
        BeanValidator.check(page);
        int count = sysUserMapper.countByDeptId(deptId);
        PageResult<SysUser> pageResult = new PageResult<>();
        if (count > 0) {
            List<SysUser> list = sysUserMapper.getPageByDeptId(deptId, page);
            pageResult.setTotal(count);
            pageResult.setData(list);
            return pageResult;
        }
        return pageResult;
    }

}
