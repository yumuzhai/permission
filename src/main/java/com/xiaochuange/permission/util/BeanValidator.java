package com.xiaochuange.permission.util;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.xiaochuange.permission.exception.ParamException;
import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

// Bean验证
public class BeanValidator {

    private static ValidatorFactory validatorFactory = Validation.buildDefaultValidatorFactory();

    /**
     * 校验单个类
     *
     * @param t
     * @param groups
     * @param <T>
     * @return
     */
    public static <T> Map<String, String> validate(T t, Class... groups) {
        Validator validator = validatorFactory.getValidator();
        Set validateResult = validator.validate(t, groups);
        if (validateResult.isEmpty()) {
            return Collections.emptyMap();
        } else {
            LinkedHashMap errorMap = Maps.newLinkedHashMap();
            Iterator iterator = validateResult.iterator();
            while (iterator.hasNext()) {
                ConstraintViolation violation = (ConstraintViolation) iterator.next();
                errorMap.put(violation.getPropertyPath().toString(), violation.getMessage());
            }
            return errorMap;
        }
    }

    /**
     * 校验一个集合
     *
     * @param collection
     * @return
     */
    public static Map<String, String> validateList(Collection<?> collection) {
        com.google.common.base.Preconditions.checkNotNull(collection);
        Iterator iterator = collection.iterator();
        Map errorMap;
        do {
            if (!iterator.hasNext()) {
                return java.util.Collections.emptyMap();
            }
            Object object = iterator.next();
            errorMap = validate(object, new Class[0]);
        } while (errorMap.isEmpty());
        return errorMap;
    }

    /**
     * @param first
     * @param objects
     * @return
     */
    public static Map<String, String> validateObject(Object first, Object... objects) {
        if (objects != null && objects.length > 0) {
            return validateList(Lists.asList(first, objects));
        } else {
            return validate(first, new Class[0]);
        }
    }

    /**
     * @param param
     * @throws ParamException
     */
    public static void check(Object param) throws ParamException {
        Map<String, String> map = BeanValidator.validateObject(param);
        // jimin if (map != null && map.entrySet().size() > 0){}
        if (org.apache.commons.collections.MapUtils.isNotEmpty(map)) {
            throw new ParamException(map.toString());
        }
    }
}
