package com.xiaochuange.permission.util;

import org.apache.commons.lang3.StringUtils;

// 层级工具类
public class LevelUtil {

    // 操作符
    public final static String SEPARATOR = ".";

    // 一级
    public final static String ROOT = "0";

    /**
     * 计算层级，如果传入的父级level为空，则是ROOT -> 0
     * -> 0
     * -> 0.1
     * -> 0.1.2
     * -> 0.1.3
     * -> 0.4
     *
     * @param parentLevel 父级级别标识
     * @param parentId    父级ID
     * @return
     */
    public static String calculateLevel(String parentLevel, int parentId) {
        if (StringUtils.isBlank(parentLevel)) {
            return ROOT;
        } else {
            return StringUtils.join(parentLevel, SEPARATOR, parentId);
        }
    }

}
