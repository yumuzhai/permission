package com.xiaochuange.permission.util;

import com.google.common.base.Splitter;
import java.util.List;
import java.util.stream.Collectors;

public class StringUtil {

    /**
     * 将字符串解析为List结构
     * ps:1,2,3,4,, => 1,2,3,4
     * 这里有个问题是：1,a,3这种解析会报错
     *
     * @param str
     * @return
     */
    public static List<Integer> splitToListInt(String str) {
        List<String> strList = Splitter.on(",").trimResults().omitEmptyStrings().splitToList(str);
        return strList.stream().map(strItem -> Integer.parseInt(strItem)).collect(Collectors.toList());
    }

}
