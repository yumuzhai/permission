package com.xiaochuange.permission.web.controller;

import com.xiaochuange.permission.common.ApplicationContextHelper;
import com.xiaochuange.permission.common.JsonData;
import com.xiaochuange.permission.dao.SysAclModuleMapper;
import com.xiaochuange.permission.exception.ParamException;
import com.xiaochuange.permission.exception.PermissionException;
import com.xiaochuange.permission.model.SysAclModule;
import com.xiaochuange.permission.param.TestVo;
import com.xiaochuange.permission.util.BeanValidator;
import com.xiaochuange.permission.util.JsonMapper;
import org.apache.commons.collections.MapUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Map;

@Controller
@RequestMapping("/test")
public class HelloController {

    private Logger log = LoggerFactory.getLogger(HelloController.class);

    @RequestMapping("/hello.json")
    @ResponseBody
    public JsonData sayHello() {
        log.info("hello");
//        throw new PermissionException("test PermissionException");
        throw new RuntimeException("test RuntimeException");
//        return JsonData.success("hello permission");
    }

    @RequestMapping("/validate.json")
    @ResponseBody
    public JsonData validate(TestVo testVo) throws ParamException {
        log.info("validate");
        /*try {
            Map<String, String> map = BeanValidator.validateObject(testVo);
            //if (map != null && map.entrySet().size() > 0) {
            if (MapUtils.isNotEmpty(map)){
                for (Map.Entry<String, String> entry : map.entrySet()) {
                    log.info("{}, {}", entry.getKey(), entry.getValue());
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }*/

        // 实际开发这么用
        BeanValidator.check(testVo);
        SysAclModuleMapper moduleMapper = ApplicationContextHelper.popBean(SysAclModuleMapper.class);
        SysAclModule sysAclModule = moduleMapper.selectByPrimaryKey(1);
        log.info(JsonMapper.obj2String(sysAclModule));
        return JsonData.success("test validate");
    }


}
