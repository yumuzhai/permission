package com.xiaochuange.permission;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;

import java.util.*;

// 判断字符串，List，Map是否为空
public class CheckNullTest {
    public static void main(String[] args) {
        Map<String, String> map = new HashMap<>();
        map.put("aa", "AA");
        map.put("bb", "BB");
        map.put("cc", "CC");
        if (map == null || map.isEmpty()) {
            System.out.println("map为空" + map);
        } else {
            System.out.println("map不为空" + map);
        }

        Multimap<String, Student> multimap = ArrayListMultimap.create();
        multimap.put("aa", new Student(1, "碧瑶1"));
        multimap.put("aa", new Student(2, "碧瑶2"));
        multimap.put("bb", new Student(3, "小凡3"));
        multimap.put("bb", new Student(4, "小凡4"));

        Iterator<Map.Entry<String, Student>> iterator = multimap.entries().iterator();
        while (iterator.hasNext()){
            Map.Entry<String, Student> next = iterator.next();
            System.out.println("key = " + next.getKey() + ", value = " + next.getValue());
        }

        Collection<Student> students = multimap.get("bb");
        for (Student student : students) {
            System.out.println(student);
        }
        // Student{id=3, name='小凡3'}
        //Student{id=4, name='小凡4'}
    }
}
